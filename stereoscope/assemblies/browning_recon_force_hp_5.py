import cadquery as cq
from assemblies.base_assembly import build_stereo_assembly
from components.shims.browning_recon_force_hp_5_shim import shim

# Holds the stereoscope, shim and camera
main_assy = cq.Assembly()

# The stereoscope sub-assembly by itself
stereo_assy = build_stereo_assembly()

# The shim to adapt the stereoscope to the camera
camera_shim = shim(orientation="vertical")

# Import the camera and fix the scanned orientation
camera = cq.importers.importStep("components/cameras/camera_trap_simplified_10k.step")
camera = camera.rotateAboutCenter((1, 0, 0), -22.5)
camera = camera.rotateAboutCenter((0, 1, 0), 42)
camera = camera.rotateAboutCenter((0, 0, 1), 9.5)

# Add the components to the assembly
main_assy.add(
    stereo_assy,
    name="stereo_assy",
    loc=cq.Location((0, 37.9, 25.95 - 1.0), (0, 0, 1), 0),
)
main_assy.add(
    camera_shim,
    name="camera_shim",
    loc=cq.Location((0, 37.9, 25.95 - (7.0 / 2.0) - 1.5), (0, 0, 1), 0),
    color=cq.Color(0.408, 0.278, 0.553, 1.0),
)
main_assy.add(camera, name="camera", color=cq.Color(0.565, 0.698, 0.278, 1.0))

show_object(main_assy)
