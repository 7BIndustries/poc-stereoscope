import cadquery as cq


def outside_mirror(
    mirror_size, mirror_thickness, arrow_face_selector=None, annotate=False
):
    """
    The mirrors that reflect the stereo images into the inside mirrors.
    """

    # The overall shape of the mirror
    mirror = cq.Workplane().box(mirror_size, mirror_size, mirror_thickness)

    # Annotation for the front and back of the mirror
    if annotate:
        mirror = mirror.faces(">Z").workplane().text("M", 12, -mirror_thickness / 3.0)

    # Tag the face to be annotated with an assembly arrow
    if arrow_face_selector != None:
        mirror.faces(arrow_face_selector).tag("arrow")

    return mirror


def inside_mirror(
    mirror_size,
    inside_mirror_size,
    mirror_thickness,
    arrow_face_selector=None,
    annotate=False,
):
    """
    The mirrors that reflect the stereo images into the camera lens.
    """

    # The overall shape of the mirror
    mirror = cq.Workplane().box(mirror_size, inside_mirror_size, mirror_thickness)

    # Annotation for the front and back of the mirror
    if annotate:
        mirror = mirror.faces(">Z").workplane().text("M", 12, -mirror_thickness / 3.0)

    # Tag the top face along the Y axis as the face to be annotated with an assembly arrow
    if arrow_face_selector != None:
        mirror.faces(arrow_face_selector).tag("arrow")

    return mirror
