import cadquery as cq

aperture_type = "rectangle"  # Can also be "single_circle"


def centered_shim():
    """
    Creates the shim that centers the stereoscope over the camera
    lens of the camera trap.
    """

    shim = cq.Workplane().box(100.0, 54.0, 7.0)

    # Cutout for PIR sensor
    shim = (
        shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox", offset=-1.0)
        .center(0.0, -24.5)
        .rect(36.75, 8.0)
        .cutThruAll()
    )

    # Cutout for lens
    shim = (
        shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox", invert=True)
        .center(0.0, 0.0)
        .circle((25.0 / 2))
        .cutBlind(2.0)
    )

    # Relief for lens cutout to keep camera from seeing side wall of shim
    if aperture_type == "single_circle":
        shim = (
            shim.faces("<Z")
            .workplane(invert=True, centerOption="CenterOfBoundBox", offset=2.0)
            .center(0.0, 0.0)
            .circle(25.0 / 2.0)
            .cutBlind(15.0, taper=-60.0)
        )

    # Handle rectangular apertures
    if aperture_type == "rectangle":
        shim = (
            shim.faces("<Z")
            .workplane(invert=True, centerOption="CenterOfBoundBox", offset=2.0)
            .pushPoints([(5.0, 0.0), (-5.0, 0.0)])
            .rect(7.0, 13.0)
            .cutThruAll()
        )

    # Cutout for detail above lens
    shim = (
        shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(0.0, 16.625)
        .rect(25.75 + 0.25, 4.25 + 0.25)
        .cutThruAll()
    )

    # Cutout for IR grid
    shim = (
        shim.faces("<Z")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .center(0.0, 26.0)
        .rect(57.0, 10.0)
        .cutBlind(2.5)
    )

    # Counter-bore holes for M3 bolts
    flange_holes = [
        (45.0, 0.0),
        (45.0, 20.0),
        (45.0, -20.0),
        (-45.0, 0.0),
        (-45.0, 20.0),
        (-45.0, -20.0),
    ]
    shim = (
        shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(flange_holes)
        .cboreHole(3.4, 6.5, 3.5)
    )

    # T loops for rubber bands
    shim = (
        shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(
            [
                (48.5, 15.0),
                (48.5, 6.0),
                (48.5, -15.0),
                (48.5, -6.0),
                (-48.5, 15.0),
                (-48.5, 6.0),
                (-48.5, -15.0),
                (-48.5, -6.0),
            ]
        )
        .rect(3.0, 1.0)
        .cutThruAll()
    )
    shim = (
        shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(
            [
                (47.5, 14.0),
                (47.5, 7.0),
                (47.5, -14.0),
                (47.5, -7.0),
                (-47.5, 14.0),
                (-47.5, 7.0),
                (-47.5, -14.0),
                (-47.5, -7.0),
            ]
        )
        .rect(1.0, 3.0)
        .cutThruAll()
    )
    shim = (
        shim.faces("<X")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .center(0.0, -2.0)
        .rect(30.0, 3.0)
        .cutBlind(3.0)
    )
    shim = (
        shim.faces(">X")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .center(0.0, -2.0)
        .rect(30.0, 3.0)
        .cutBlind(3.0)
    )

    return shim


def zip_tie_aperture_shim():
    """
    Creates the shim that centers the stereoscope over the camera
    lens of the camera trap.
    """

    shim = cq.Workplane().box(100.0, 54.0, 7.0)

    # Cutout for PIR sensor
    shim = (
        shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox", offset=-1.0)
        .center(0.0, -24.5)
        .rect(36.75, 8.0)
        .cutThruAll()
    )

    # Cutout for lens
    shim = (
        shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox", invert=True)
        .center(0.0, 0.0)
        .circle((25.0 / 2))
        .cutBlind(2.0)
    )

    # Relief for lens cutout to keep camera from seeing side wall of shim
    if aperture_type == "single_circle":
        shim = (
            shim.faces("<Z")
            .workplane(invert=True, centerOption="CenterOfBoundBox", offset=2.0)
            .center(0.0, 0.0)
            .circle(25.0 / 2.0)
            .cutBlind(15.0, taper=-60.0)
        )

    # Handle rectangular apertures
    if aperture_type == "rectangle":
        shim = (
            shim.faces("<Z")
            .workplane(invert=True, centerOption="CenterOfBoundBox", offset=2.0)
            .pushPoints([(5.0, 0.0), (-5.0, 0.0)])
            .rect(7.0, 13.0)
            .cutThruAll()
        )

    # Cutout for detail above lens
    shim = (
        shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(0.0, 16.625)
        .rect(25.75 + 0.25, 4.25 + 0.25)
        .cutThruAll()
    )

    # Cutout for IR grid
    shim = (
        shim.faces("<Z")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .center(0.0, 26.0)
        .rect(57.0, 10.0)
        .cutBlind(2.5)
    )

    # Counter-bore holes for M3 bolts
    flange_holes = [
        (45.0, 0.0),
        (45.0, 20.0),
        (45.0, -20.0),
        (-45.0, 0.0),
        (-45.0, 20.0),
        (-45.0, -20.0),
    ]
    shim = (
        shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(flange_holes)
        .cboreHole(3.4, 6.5, 3.5)
    )

    # Wings for zip-ties
    shim = (
        shim.faces(">Y")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints([(42.5, 0),
                     (-42.5, 0)])
        .rect(15.0, 7.0)
        .extrude(15)
    )
    shim = (
        shim.faces("<Y")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints([(42.5, 0),
                     (-42.5, 0)])
        .rect(15.0, 7.0)
        .extrude(15)
    )

    # Holes for zip-ties
    shim = (
        shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints([(42.5, 32.5),
                     (-42.5, 32.5),
                     (42.5, -32.5),
                     (-42.5, -32.5)])
        .rect(7, 12)
        .cutThruAll()
    )

    return shim


def horizotal_offset_shim():
    """
    Creates the shim that centers the stereoscope over the camera
    lens of the camera trap.
    """

    shim = cq.Workplane().box(100.0, 57.0, 9.0)

    # Cutout for PIR sensor
    shim = (
        shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(0.0, -24.5)
        .rect(36.75, 8.0)
        .cutThruAll()
    )

    # Cutout for lens
    shim = (
        shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(0.0, 0.0)
        .circle((25.0 / 2))
        .cutThruAll()
    )

    # Relief for lens cutout to keep camera from seeing side wall of shim
    shim = (
        shim.faces("<Z")
        .workplane(invert=True, centerOption="CenterOfBoundBox", offset=2.0)
        .center(0.0, 0.0)
        .circle(25.0 / 2.0)
        .cutBlind(15.0, taper=-60.0)
    )

    # Cutout for detail above lens
    shim = (
        shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(0.0, 16.625)
        .rect(25.75 + 0.25, 4.25 + 0.25)
        .cutThruAll()
    )

    # Cutout for IR grid
    shim = (
        shim.faces("<Z")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .center(0.0, 26.0)
        .rect(57.0, 10.0)
        .cutBlind(2.5)
    )

    # Counter-bore holes for M3 bolts
    flange_holes = [
        # (44.0, 0.0),
        (42.0, 20.0),
        (42.0, -20.0),
        # (-46.0, 0.0),
        (-48.0, 20.0),
        (-48.0, -20.0),
    ]
    shim = (
        shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(flange_holes)
        .cboreHole(3.4, 6.5, 3.5)
    )

    # T loops for rubber bands
    shim = (
        shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(
            [
                (48.5, 15.0),
                (48.5, 6.0),
                (48.5, -15.0),
                (48.5, -6.0),
                (-48.5, 15.0),
                (-48.5, 6.0),
                (-48.5, -15.0),
                (-48.5, -6.0),
            ]
        )
        .rect(3.0, 1.0)
        .cutThruAll()
    )
    shim = (
        shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(
            [
                (47.5, 14.0),
                (47.5, 7.0),
                (47.5, -14.0),
                (47.5, -7.0),
                (-47.5, 14.0),
                (-47.5, 7.0),
                (-47.5, -14.0),
                (-47.5, -7.0),
            ]
        )
        .rect(1.0, 3.0)
        .cutThruAll()
    )
    shim = (
        shim.faces("<X")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .center(0.0, -4.0)
        .rect(30.0, 6.0)
        .cutBlind(3.0)
    )
    shim = (
        shim.faces(">X")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .center(0.0, -4.0)
        .rect(30.0, 6.0)
        .cutBlind(3.0)
    )

    return shim


def thin_shim():
    """
    Creates the thin shim that centers the stereoscope over the
    lens of the camera trap.
    """

    shim = cq.Workplane().box(100.0, 57.0, 5.0)

    # Cutout for PIR sensor
    # shim = (
    #     shim.faces(">Z")
    #     .workplane(centerOption="CenterOfBoundBox")
    #     .center(0.0, -24.5)
    #     .rect(36.75, 8.0)
    #     .cutThruAll()
    # )

    # Cutout for lens
    shim = (
        shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(0.0, -7.80)
        .circle((25.0 / 2))
        .cutThruAll()
    )

    # Relief for lens cutout to keep camera from seeing side wall of shim
    shim = (
        shim.faces("<Z")
        .workplane(invert=True, centerOption="CenterOfBoundBox", offset=2.0)
        .center(0.0, -7.80)
        .circle(25.0 / 2.0)
        .cutBlind(15.0, taper=-60.0)
    )

    # Cutout for detail above lens
    shim = (
        shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(0.0, 16.625 - 7.80)
        .rect(25.75 + 0.25, 4.25 + 0.25)
        .cutThruAll()
    )

    # Cutout for IR grid
    shim = (
        shim.faces("<Z")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .center(0.0, 26.0)
        .rect(57.0, 19.0)
        .cutBlind(2.5)
    )

    # Counter-bore holes for M3 bolts
    flange_holes = [
        # (45.0, 0.0),
        (45.0, 20.0),
        (45.0, -20.0),
        # (-45.0, 0.0),
        (-45.0, 20.0),
        (-45.0, -20.0),
    ]
    shim = (
        shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(flange_holes)
        .cboreHole(3.4, 6.5, 3.5)
    )

    # T loops for rubber bands
    shim = (
        shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(
            [
                (48.5, 15.0),
                (48.5, 6.0),
                (48.5, -15.0),
                (48.5, -6.0),
                (-48.5, 15.0),
                (-48.5, 6.0),
                (-48.5, -15.0),
                (-48.5, -6.0),
            ]
        )
        .rect(3.0, 1.0)
        .cutThruAll()
    )
    shim = (
        shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(
            [
                (47.5, 14.0),
                (47.5, 7.0),
                (47.5, -14.0),
                (47.5, -7.0),
                (-47.5, 14.0),
                (-47.5, 7.0),
                (-47.5, -14.0),
                (-47.5, -7.0),
            ]
        )
        .rect(1.0, 3.0)
        .cutThruAll()
    )
    shim = (
        shim.faces("<X")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .center(0.0, -2.0)
        .rect(30.0, 3.0)
        .cutBlind(3.0)
    )
    shim = (
        shim.faces(">X")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .center(0.0, -2.0)
        .rect(30.0, 3.0)
        .cutBlind(3.0)
    )

    return shim


if "show_object" in globals():
    shim = centered_shim()

    show_object(shim)


def main():
    from cadquery.vis import show

    shim = centered_shim()

    show(shim)


if __name__ == "__main__":
    main()
