import cadquery as cq

orientation = "horizontal"


def shim(orientation="horizontal"):
    """
    Allows the caller to select between a "horizontal" shim and a "vertical" shim.
    """

    if orientation == "horizontal":
        shim = horizontal_shim()
    else:
        shim = vertical_shim()

    return shim


def horizontal_shim():
    """
    Shim to adapt the stereoscope to a Browning Recon Force HP 5 camera trap.
    """

    # Main shape
    camera_shim = cq.Workplane().box(100.0, 54.0, 8.0)

    # Central cutout
    camera_shim = (
        camera_shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(0, 10)
        .rect(80, 50)
        .cutThruAll()
    )

    # T-slots for rubber bands
    camera_shim = (
        camera_shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(
            [
                (48.5, 15.0),
                (48.5, 6.0),
                (48.5, -15.0),
                (48.5, -6.0),
                (-48.5, 15.0),
                (-48.5, 6.0),
                (-48.5, -15.0),
                (-48.5, -6.0),
            ]
        )
        .rect(3.0, 1.0)
        .cutThruAll()
    )
    camera_shim = (
        camera_shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(
            [
                (47.5, 14.0),
                (47.5, 7.0),
                (47.5, -14.0),
                (47.5, -7.0),
                (-47.5, 14.0),
                (-47.5, 7.0),
                (-47.5, -14.0),
                (-47.5, -7.0),
            ]
        )
        .rect(1.0, 3.0)
        .cutThruAll()
    )

    # Counter-bore holes for M3 bolts
    flange_holes = [
        (45.0, 0.0),
        (45.0, 20.0),
        (45.0, -20.0),
        (-45.0, 0.0),
        (-45.0, 20.0),
        (-45.0, -20.0),
    ]
    camera_shim = (
        camera_shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(flange_holes)
        .cboreHole(3.4, 6.5, 3.5)
    )

    # Clearance for rubber bands to be hooked on t-slots
    camera_shim = (
        camera_shim.faces("<X")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .center(0.0, -2.0)
        .rect(30.0, 4.0)
        .cutBlind(2.0)
    )
    camera_shim = (
        camera_shim.faces(">X")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .center(0.0, -2.0)
        .rect(30.0, 4.0)
        .cutBlind(2.0)
    )

    return camera_shim
