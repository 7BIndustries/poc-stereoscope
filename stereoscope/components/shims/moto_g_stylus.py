import cadquery as cq


def camera_shim():
    """
    The mounting shim for the camera.
    """

    # The base shim block
    shim = cq.Workplane().box(50.0, 39.2, 5.0)

    # The center cutout for the camera boss
    shim = shim.faces(">Z").workplane(invert=True).rect(25.8, 32.8).cutThruAll()

    # Alignment stud
    shim = (
        shim.faces(">Z")
        .workplane(centerOption="CenterOfMass")
        .moveTo(9.9, 9.6)
        .rect(8.1, 8.1)
        .extrude(1.75)
    )

    return shim
