import cadquery as cq

orientation = "horizontal"
fastener_type = "zip_tie"  # rubber_band or zip_tie


def shim(orientation="horizontal", fastener_type="zip_tie"):
    """
    Allows the caller to select between a "horizontal" shim and a "vertical" shim.
    :param orientation: Can be either "horizontal" or "vertical"
    :param fastener_type: Can be eigher "rubber_band" or "zip_tie"
    """

    if orientation == "horizontal":
        shim = horizontal_shim(fastener_type)
    else:
        shim = vertical_shim(fastener_type)

    return shim


def horizontal_shim(fastener_type):
    """
    Shim to adapt the stereoscope to a Browning Recon Force HP 5 camera trap.
    """

    # Overall dimensions
    width = 100.0
    height = 54.0
    thickness = 8.0

    # Main shape
    camera_shim = cq.Workplane().box(width, height, thickness)

    # Center hole
    camera_shim = (
        camera_shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(-1.0, 4.0)
        .hole(36.5)
    )

    # Add bottom flat
    camera_shim = (
        camera_shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(-1.0, -12.85)
        .rect(36.5, 3.75)
        .extrude(-8.0)
    )

    # Add top flat
    camera_shim = (
        camera_shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(-1.0, 20.0)
        .rect(36.5, 6.0)
        .extrude(-8.0)
    )

    # Clearance cutout
    camera_shim = (
        camera_shim.faces("<Y")
        .workplane(centerOption="CenterOfBoundBox")
        .center(0.0, 0.0)
        .rect(40.0, 8.0)
        .cutBlind(-1.0)
    )

    # Counter-bore holes for M3 bolts
    flange_holes = [
        (45.0, 0.0),
        (45.0, 20.0),
        (45.0, -20.0),
        (-45.0, 0.0),
        (-45.0, 20.0),
        (-45.0, -20.0),
    ]
    camera_shim = (
        camera_shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(flange_holes)
        .cboreHole(3.4, 6.5, 3.5)
    )

    # Modify the design based on the fastening method to the camera trap
    if fastener_type == "rubber_band":
        # Clearance for rubber bands to be hooked on t-slots
        camera_shim = (
            camera_shim.faces("<X")
            .workplane(invert=True, centerOption="CenterOfBoundBox")
            .center(0.0, -2.0)
            .rect(30.0, 4.0)
            .cutBlind(2.0)
        )
        camera_shim = (
            camera_shim.faces(">X")
            .workplane(invert=True, centerOption="CenterOfBoundBox")
            .center(0.0, -2.0)
            .rect(30.0, 4.0)
            .cutBlind(2.0)
        )

        # T-slots for rubber bands
        camera_shim = (
            camera_shim.faces("<Z")
            .workplane(centerOption="CenterOfBoundBox")
            .pushPoints(
                [
                    (48.5, 15.0),
                    (48.5, 6.0),
                    (48.5, -15.0),
                    (48.5, -6.0),
                    (-48.5, 15.0),
                    (-48.5, 6.0),
                    (-48.5, -15.0),
                    (-48.5, -6.0),
                ]
            )
            .rect(3.0, 1.0)
            .cutThruAll()
        )
        camera_shim = (
            camera_shim.faces("<Z")
            .workplane(centerOption="CenterOfBoundBox")
            .pushPoints(
                [
                    (47.5, 14.0),
                    (47.5, 7.0),
                    (47.5, -14.0),
                    (47.5, -7.0),
                    (-47.5, 14.0),
                    (-47.5, 7.0),
                    (-47.5, -14.0),
                    (-47.5, -7.0),
                ]
            )
            .rect(1.0, 3.0)
            .cutThruAll()
        )
    else:
        # Bottom wings for zip-ties
        camera_shim = (
            camera_shim.faces("<Y")
            .workplane(centerOption="CenterOfBoundBox")
            .pushPoints([(42.5, 0),
                        (-42.5, 0)])
            .rect(15.0, 8.0)
            .extrude(20.0)
        )

        # Side wings for zip-ties
        camera_shim = (
            camera_shim.faces(">Y")
            .workplane(centerOption="CenterOfBoundBox", offset=-(height / 2.0 + 10.0))
            .pushPoints([(width / 2.0 + 6.0, 0),
                        (-width / 2.0 - 6.0, 0)])
            .rect(15.0, 8.0)
            .extrude(20.0)
        )

        # Bottom holes for zip-ties
        camera_shim = (
            camera_shim.faces(">Z")
            .workplane(centerOption="CenterOfBoundBox")
            .pushPoints([
                        (42.5, -26.5),
                        (-42.5, -26.5)
                        ])
            .rect(7, 12)
            .cutThruAll()
        )

        # Side holes for zip-ties
        camera_shim = (
            camera_shim.faces(">Z")
            .workplane(centerOption="CenterOfBoundBox")
            .pushPoints([
                        (width / 2.0 + 6.0, 10.0),
                        (-width / 2.0 - 6.0, 10.0)
                        ])
            .rect(7, 12)
            .cutThruAll()
        )

    return camera_shim


def vertical_shim(fastener_type):
    """
    Vertical variant of the shim to adapt the stereoscope to a Browning Recon Force HP 5 camera trap.
    """

    # Overall dimensions
    width = 100.0
    height = 54.0
    thickness = 8.0

    # Main shape
    camera_shim = cq.Workplane().box(width, height, thickness)

    # Change the orientation to make assembly more seamless
    camera_shim = camera_shim.rotateAboutCenter((0, 0, 1), 90)

    # Center hole
    camera_shim = (
        camera_shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(-1.0, 4.0)
        .hole(36.5)
    )

    # Add bottom flat
    camera_shim = (
        camera_shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(-1.0, -12.85)
        .rect(36.5, 3.75)
        .extrude(-8.0)
    )

    # Add top flat
    camera_shim = (
        camera_shim.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(-1.0, 20.0)
        .rect(36.5, 6.0)
        .extrude(-8.0)
    )

    # Add cutout for PIR sensor
    camera_shim = (
        camera_shim.faces("<Y")
        .workplane(centerOption="CenterOfBoundBox")
        .center(-2.75, 0.0)
        .rect(39.0, 8.0)
        .cutBlind(-25.5)
    )

    # Modify the design based on the fastening method to the camera trap
    if fastener_type == "rubber_band":
        # Clearance for rubber bands to be hooked on t-slots
        camera_shim = (
            camera_shim.faces("<X")
            .workplane(invert=True, centerOption="CenterOfBoundBox")
            .center(0.0, -2.0)
            .rect(75.0, 4.0)
            .cutBlind(2.0)
        )
        camera_shim = (
            camera_shim.faces(">X")
            .workplane(invert=True, centerOption="CenterOfBoundBox")
            .center(0.0, -2.0)
            .rect(75.0, 4.0)
            .cutBlind(2.0)
        )

        # T-slots for rubber bands
        camera_shim = (
            camera_shim.faces("<Z")
            .workplane(centerOption="CenterOfBoundBox")
            .pushPoints(
                [
                    (-(54.0 / 2.0) + (3.0 / 2.0), -7.0),
                    ((54.0 / 2.0) - (3.0 / 2.0), -7.0),
                    (-(54.0 / 2.0) + (3.0 / 2.0), 0.0),
                    ((54.0 / 2.0) - (3.0 / 2.0), 0.0),
                    (-(54.0 / 2.0) + (3.0 / 2.0), 7.0),
                    ((54.0 / 2.0) - (3.0 / 2.0), 7.0),
                    (-(54.0 / 2.0) + (3.0 / 2.0), 14.0),
                    ((54.0 / 2.0) - (3.0 / 2.0), 14.0),
                    (-(54.0 / 2.0) + (3.0 / 2.0), 21.0),
                    ((54.0 / 2.0) - (3.0 / 2.0), 21.0),
                    (-(54.0 / 2.0) + (3.0 / 2.0), 28.0),
                    ((54.0 / 2.0) - (3.0 / 2.0), 28.0),
                    (-(54.0 / 2.0) + (3.0 / 2.0), 35.0),
                    ((54.0 / 2.0) - (3.0 / 2.0), 35.0),
                ]
            )
            .rect(3.0, 1.0)
            .cutThruAll(100.0)
        )
        camera_shim = (
            camera_shim.faces("<Z")
            .workplane(centerOption="CenterOfBoundBox")
            .pushPoints(
                [
                    (-(54.0 / 2.0) + (3.0 / 2.0) + 1.0, -7.0),
                    ((54.0 / 2.0) - (3.0 / 2.0) - 1.0, -7.0),
                    (-(54.0 / 2.0) + (3.0 / 2.0) + 1.0, 0.0),
                    ((54.0 / 2.0) - (3.0 / 2.0) - 1.0, 0.0),
                    (-(54.0 / 2.0) + (3.0 / 2.0) + 1.0, 7.0),
                    ((54.0 / 2.0) - (3.0 / 2.0) - 1.0, 7.0),
                    (-(54.0 / 2.0) + (3.0 / 2.0) + 1.0, 14.0),
                    ((54.0 / 2.0) - (3.0 / 2.0) - 1.0, 14.0),
                    (-(54.0 / 2.0) + (3.0 / 2.0) + 1.0, 21.0),
                    ((54.0 / 2.0) - (3.0 / 2.0) - 1.0, 21.0),
                    (-(54.0 / 2.0) + (3.0 / 2.0) + 1.0, 28.0),
                    ((54.0 / 2.0) - (3.0 / 2.0) - 1.0, 28.0),
                    (-(54.0 / 2.0) + (3.0 / 2.0) + 1.0, 35.0),
                    ((54.0 / 2.0) - (3.0 / 2.0) - 1.0, 35.0),
                ]
            )
            .rect(1.0, 3.0)
            .cutThruAll()
        )
    else:
        # Wings for zip-ties
        camera_shim = (
            camera_shim.faces(">Z")
            .workplane(centerOption="CenterOfBoundBox")
            .pushPoints([(height / 2.0 + 10.0, 0.0),
                        (-height / 2.0  - 10.0, 0.0),
                        (height / 2.0 + 10.0, -width / 2.0 + 10.0),
                        (-height / 2.0  - 10.0, -width / 2.0 + 10.0)])
            .rect(25.0, 20.0)
            .extrude(-8.0)
        )

        # Holes for zip-ties
        camera_shim = (
            camera_shim.faces(">Z")
            .workplane(centerOption="CenterOfBoundBox")
            .pushPoints([(height / 2.0 + 12.5, 0.0),
                        (-height / 2.0  - 12.5, 0.0),
                        (height / 2.0 + 12.5, -width / 2.0 + 10.0),
                        (-height / 2.0  - 12.5, -width / 2.0 + 10.0)])
            .rect(7, 12)
            .cutThruAll()
        )

    # Counter-bore holes for M3 bolts
    flange_holes = [
        (20.0, 45.0),
        (0.0, -45.0),
        (20.0, -45.0),
        (-20.0, -45.0),
    ]
    camera_shim = (
        camera_shim.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(flange_holes)
        .cboreHole(3.4, 6.5, 3.5)
    )

    return camera_shim
