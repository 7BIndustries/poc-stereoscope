import cadquery as cq
from parameters import (
    inside_projected_length,
    projected_length,
    mirror_size,
    outside_mirror_ext_offset,
    inside_mirror_angle,
    outside_mirror_angle,
    mirror_thickness,
)

# Primary parameters
camera_boss_thickness = 2.0
base_bottom_thickness = 2.0
slot_fit_tolerance = 0.35  # Sets how tight or loose the mirrors fit in the slots


def base():
    """
    Base that holds the mirrors, adjusted so that the mirror planes do not overlap.
    """

    # Width to enclose the inside and outside mirrors
    center_width = (inside_projected_length / 2.0 + projected_length / 2.0 + 6.0) * 2.0

    # Basic block
    base = (
        cq.Workplane()
        .rect(mirror_size + 4.0, center_width)
        .workplane(offset=mirror_size - 5.0)
        .rect(
            mirror_size + 4.0,
            (
                projected_length
                + inside_projected_length
                + outside_mirror_ext_offset
                + 7.0
            )
            * 2.0,
        )
        .loft()
    )

    # Mounting flange to interface with camera shim
    base = (
        base.faces("<Z")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .rect(mirror_size + 4.0, 100.0)
        .extrude(5.0)
    )

    # Mounting holes in flange that tie shim to stereoscope
    flange_holes = [
        (0.0, 45.0),
        (20.0, 45.0),
        (-20.0, 45.0),
        (0.0, -45.0),
        (20.0, -45.0),
        (-20.0, -45.0),
    ]  # Positions of all of the holes/nut traps in the flange
    base = (
        base.faces("<Z[-2]")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .pushPoints(flange_holes)
        .circle(3.4 / 2.0)
        .cutBlind(10.0)
    )
    base = (
        base.faces("<Z[-2]")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .pushPoints(flange_holes)
        .polygon(6, 6.40)
        .cutBlind(2.3)
    )

    # Overall box cutout
    base = (
        base.faces("<Z")
        .workplane(invert=True)
        .rect(mirror_size - 3.5, center_width - 4.0)
        .workplane(offset=mirror_size - 5.0)
        .rect(
            mirror_size - 4.0,
            (
                projected_length
                + inside_projected_length
                + outside_mirror_ext_offset
                + 8.0
            )
            * 2.0
            - 7.0,
        )
        .loft(combine="cut")
    )

    # Center area cutout
    base = (
        base.faces(">Z")
        .workplane(invert=True)
        .rect(mirror_size + 4.0, mirror_size + 69.0)
        .workplane(offset=mirror_size / 3.0)
        .rect(mirror_size + 4.0, mirror_size - 3.0)
        .loft(combine="cut")
    )

    # Inner mirror slots
    base = (
        base.faces("<Z")
        .workplane(
            centerOption="CenterOfBoundBox", invert=True, offset=base_bottom_thickness
        )
        .transformed(
            offset=cq.Vector(0.0, 0.0, 0.0),
            rotate=cq.Vector(inside_mirror_angle - 90, 0, 0),
        )
        .rect(mirror_size + slot_fit_tolerance, mirror_thickness + slot_fit_tolerance)
        .extrude(mirror_size + 15.0, combine="cut")
    )
    base = (
        base.faces("<Z")
        .workplane(
            centerOption="CenterOfBoundBox", invert=True, offset=base_bottom_thickness
        )
        .transformed(
            offset=cq.Vector(0.0, 0.0, 0.0),
            rotate=cq.Vector(-(inside_mirror_angle - 90), 0, 0),
        )
        .rect(mirror_size + slot_fit_tolerance, mirror_thickness + slot_fit_tolerance)
        .extrude(mirror_size + 15.0, combine="cut")
    )

    # Outer mirror slots
    base = (
        base.faces("<Z")
        .workplane(
            centerOption="CenterOfBoundBox", invert=True, offset=base_bottom_thickness
        )
        .transformed(
            offset=cq.Vector(
                0.0,
                -inside_projected_length / 2.0
                - projected_length / 2.0
                - outside_mirror_ext_offset,
                0.0,
            ),
            rotate=cq.Vector(90 - outside_mirror_angle, 0, 0),
        )
        .rect(mirror_size + slot_fit_tolerance, mirror_thickness + slot_fit_tolerance)
        .extrude(mirror_size + 10.0, combine="cut")
    )
    base = (
        base.faces("<Z")
        .workplane(
            centerOption="CenterOfBoundBox", invert=True, offset=base_bottom_thickness
        )
        .transformed(
            offset=cq.Vector(
                0.0,
                inside_projected_length / 2.0
                + projected_length / 2.0
                + outside_mirror_ext_offset,
                0.0,
            ),
            rotate=cq.Vector(-(90 - outside_mirror_angle), 0, 0),
        )
        .rect(mirror_size + slot_fit_tolerance, mirror_thickness + slot_fit_tolerance)
        .extrude(mirror_size + 10.0, combine="cut")
    )

    # Attachment holes for center piece
    base = (
        base.faces("<X[-2]")
        .workplane(centerOption="CenterOfBoundBox")
        .move(0.0, -4.5)
        .circle(3.4 / 2.0)
        .cutThruAll()
    )

    return base
