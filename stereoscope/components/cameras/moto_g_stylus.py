import cadquery as cq


def phone():
    """
    Creates a rough approximation of a Moto G Stylus phone that can be used
    to capture stereo images.
    """

    # Basic block shape
    phone = cq.Workplane().box(77.5, 169.5, 9.4)

    # Raised camera  boss
    phone = (
        phone.faces("<Z")
        .workplane()
        .moveTo(
            -(77.5 / 2.0) + (26.0 / 2.0) + 6.10, (169.5 / 2.0) - (33.0 / 2.0) - 8.0,
        )
        .rect(26.0, 33.0)
        .extrude(0.70)
    )
    phone = (
        phone.faces("<Z")
        .workplane()
        .moveTo(
            -(77.5 / 2.0) + (26.0 / 2.0) + 6.10,
            (169.5 / 2.0) - (33.0 / 2.0) - 8.0 + 3.5,
        )
        .rect(24.0, 24.0)
        .extrude(0.65)
    )

    # Add the camera bumps
    phone = (
        phone.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .rect(12.225, 12.225, forConstruction=True)
        .vertices()
        .circle(4.7625)
        .extrude(0.4)
    )

    # Rounds on camera boss
    phone = phone.faces(">Y[-2]").edges("|Z").fillet(9.525 / 2.0)
    phone = phone.faces(">Y[-3]").edges("|Z").fillet(9.525 / 2.0)
    phone = phone.faces(">Y[-4]").edges("|Z").fillet(9.525 / 2.0)
    phone = phone.faces(">Y[-5]").edges("|Z").fillet(9.525 / 2.0)

    # Add the corner rounds
    phone = phone.faces(">Y").edges("|Z").fillet(15.875 / 2.0)
    phone = phone.faces("<Y").edges("|Z").fillet(15.875 / 2.0)

    # Bottom rounds
    # phone = phone.faces("<Z").edges("|Y").fillet(6.35)

    return phone
