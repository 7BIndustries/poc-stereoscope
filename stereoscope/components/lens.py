import cadquery as cq
from parameters import mirror_size, projected_length


def lens():
    """
    Lens that seals up the front face of the stereoscope.
    """

    ls = cq.Workplane().box(mirror_size - 4.0 + 5.0, projected_length + 6.0, 1.0)

    return ls
