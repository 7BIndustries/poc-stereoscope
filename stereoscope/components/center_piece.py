import cadquery as cq


def center_piece(
    mirror_size, inside_mirror_size, inside_projected_length, arrow_face_selector=None
):
    """
    Piece that blocks the mirror paths in the center of the stereoscope.
    """

    # Tapered shape
    center = (
        cq.Workplane()
        .rect(mirror_size - 3.5, 3.0)
        .workplane(offset=inside_projected_length - 3.75)
        .rect(mirror_size - 3.5, inside_mirror_size + 11.5)
        .loft()
    )

    # Nut traps for side mounting holes
    center = (
        center.faces(">Z")
        .workplane(invert=True)
        .pushPoints([(-20.0, 0.0), (20.0, 0.0)])
        .rect(2.75, 5.75)
        .extrude(14.5, combine="cut")
    )

    # M3 mouting holes in side
    center = (
        center.faces(">X")
        .workplane(centerOption="CenterOfBoundBox", invert=True)
        .circle(1.7)
        .extrude(12.0, combine="cut")
    )
    center = (
        center.faces("<X")
        .workplane(centerOption="CenterOfBoundBox", invert=True)
        .circle(1.7)
        .extrude(12.0, combine="cut")
    )

    # M3 mouting holes with nut traps for stereoscope front
    front_mounting_holes = [(-7.0, 0.0), (7.0, 0.0)]
    center = (
        center.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox", invert=True)
        .pushPoints(front_mounting_holes)
        .polygon(6, 6.4)
        .extrude(20.0, combine="cut")
    )
    center = (
        center.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(front_mounting_holes)
        .hole(3.4)
    )

    # Tag the top face along the Z axis as the face to be annotated with an assembly arrow
    if arrow_face_selector != None:
        center.faces(arrow_face_selector).tag("arrow")

    return center
