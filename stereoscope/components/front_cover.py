import cadquery as cq
from parameters import mirror_size, projected_length


def front_cover(arrow_face_selector=None):
    """
    Creates a front cover for the stereoscope to protect and hold the mirrors in place.
    """

    # Draw the overall shape in 2D
    cover = (
        cq.Workplane("YZ")
        .lineTo(47.0 / 2.0, 0.0)
        .lineTo(59.5, 16.75)
        .hLine(6.05)
        .vLine(5.0)
        .hLine(-6.05)
        .lineTo(47.0 / 2.0 - 2.12, 5.0)
        .lineTo(0.0, 5.0)
        .wire()
        .mirrorY()
        .extrude(mirror_size + 4.0)
    )

    # Center piece mounting holes
    cover = (
        cover.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints([(-7.0, 0.0), (7.0, 0.0)])
        .hole(3.4)
    )

    # Rectangular holes to let the mirror paths through
    cover = (
        cover.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox", invert=True)
        .pushPoints([(0.0, -mirror_size + 5.0), (0.0, mirror_size - 5.0)])
        .rect(mirror_size - 4.0, projected_length - 4.0)
        .cutThruAll()
    )

    # Tag the face of the screw so that an assembly arrow can be added
    if arrow_face_selector != None:
        cover.faces(arrow_face_selector).tag("arrow")

    # Add the insets for plexiglass lenses
    cover = (
        cover.faces("|(0.0, 0.4022797995533619, 0.9155167736700989)")
        .workplane(centerOption="CenterOfBoundBox")
        .move(0.0, 4.5)
        .rect(mirror_size - 4.0 + 5.0, projected_length + 8.0)
        .extrude(-1.0, combine="cut")
    )
    cover = (
        cover.faces("|(0.0, -0.4022797995533619, 0.9155167736700989)")
        .workplane(centerOption="CenterOfBoundBox")
        .move(0.0, 4.5)
        .rect(mirror_size - 4.0 + 5.0, projected_length + 8.0)
        .extrude(-1.0, combine="cut")
    )

    return cover


if "show_object" in globals():
    front = front_cover(arrow_face_selector=None)

    show_object(front)


def main():
    from cadquery.vis import show

    front = front_cover(arrow_face_selector=None)

    show(front)


if __name__ == "__main__":
    main()
