import cadquery as cq


def m3x12_bolt(arrow_face_selector=None):
    """
    Creates an approximation of an M3x12mm bolt.
    """

    # Bolt head
    bolt = cq.Workplane().circle(5.68 / 2.0).extrude(3.0)

    # Bolt shaft
    bolt = bolt.faces(">Z").circle(3.0 / 2.0).extrude(12.0)

    # Hex key pocket
    bolt = bolt.faces("<Z").workplane(invert=True).polygon(6, 3.0).cutBlind(1.5)

    # Round the top of the head
    bolt = bolt.faces("<Z").edges("%CIRCLE").fillet(0.25)

    # Tag the top face along the Z axis as the face to be annotated with an assembly arrow
    if arrow_face_selector != None:
        bolt.faces(arrow_face_selector).tag("arrow")

    return bolt


def m3_nut():
    """
    Creates an approximation of an M3 hex nut.
    """

    # Main shape
    nut = cq.Workplane().polygon(6, 6.4).extrude(2.5)

    nut = nut.edges().fillet(0.25)

    # Center hole
    nut = nut.faces(">Z").workplane(centerOption="CenterOfBoundBox").hole(3.0)

    # Tag the top face along the Z axis as the face to be annotated with an assembly arrow
    nut.faces(">Z").tag("arrow")

    return nut


def m3_square_nut():
    """
    Creates an approximation of an M3 square nut.
    """

    # Main shape
    nut = cq.Workplane().rect(5.5, 5.5).extrude(2.5)

    # Center hole
    nut = nut.faces(">Z").workplane(centerOption="CenterOfBoundBox").hole(3.0)

    # Tag a face to have the assembly arrow point to it
    nut.faces(">Y").tag("arrow")

    return nut
