from math import cos, radians

mirror_size = 50.0
inside_mirror_size = 38.0  # Old ratio = (mirror_size * 0.76)
mirror_thickness = 1.06
inside_mirror_angle = 135.0
outside_mirror_angle = 55.0
outside_mirror_ext_offset = (
    3.0  # Sets how far past "perfect distance" the outer mirrors should be pushed out
)

# Derived parameters
projected_length = mirror_size * cos(radians(outside_mirror_angle))
inside_projected_length = inside_mirror_size * cos(radians(inside_mirror_angle - 90.0))
base_length = (inside_projected_length + projected_length) * 2.0
base_width = mirror_size + 5.0


def get_global_params():
    """
    Returns a dictionary of the global parameters that can be passed around more easily.
    """

    global_params = {}
    global_params["mirror_size"] = mirror_size
    global_params["inside_mirror_size"] = inside_mirror_size
    global_params["mirror_thickness"] = mirror_thickness
    global_params["inside_mirror_angle"] = inside_mirror_angle
    global_params["outside_mirror_angle"] = outside_mirror_angle
    global_params["outside_mirror_ext_offset"] = outside_mirror_ext_offset
    global_params["projected_length"] = projected_length
    global_params["inside_projected_length"] = inside_projected_length
    global_params["base_length"] = base_length
    global_params["base_width"] = base_width

    return global_params
