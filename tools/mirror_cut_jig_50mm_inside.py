import cadquery as cq
import tools.helpers  # Add the parent directory to Python path regardless of execution environment
from cq_annotate import explode_assembly, add_assembly_arrows

# Display parameters
explode = True
annotate = True


def mirror_cutout(mirror_size, mirror_thickness):
    """
    The slots in the box that the mirrors fit into.
    """
    return cq.Workplane().box(
        mirror_size + 0.5, mirror_size * 2, mirror_thickness + 0.5
    )


def jig(global_params):
    """
    Generates the jig that holds the mirror in position so that a straight
    and accurate cut can be performed.
    """

    # Unpack the global parameters
    mirror_size = global_params["mirror_size"]
    mirror_thickness = global_params["mirror_thickness"]

    # Overall shape
    jig = cq.Workplane().box(mirror_size + 10, mirror_size - 5, mirror_thickness + 2.5)

    # Mirror cutout
    mco = mirror_cutout(mirror_size, mirror_thickness)
    mco.val().locate(cq.Location((0, mirror_size / 2.0 + 3.5, 0)))
    jig = jig.cut(mco)

    # Glass cutter guide channel
    jig = (
        jig.faces(">Z")
        .workplane(invert=True)
        .move(0, (-(mirror_size * 0.76) / 2.0) + 2.5)
        .rect(mirror_size + 10, 1.5)
        .cutBlind(1.5)
    )

    # Cutout to allow pushing the glass panel back out
    jig = (
        jig.faces("<Y")
        .workplane(centerOption="CenterOfBoundBox", invert=True)
        .rect(10, 10)
        .cutBlind(5.0)
    )

    return jig


def jig_assembly(global_params, outside_mirror, arrow_selector=">Y", annotate=False):
    """
    Generates the jig assembly with all the parts in the correct place.
    """

    jig_assy = cq.Assembly()

    # Add the cut jig itself
    jig_assy.add(
        jig(global_params),
        name="jig",
        color=cq.Color(1.0, 0.0, 0.0, 0.5),
        metadata={"export_format": "stl"},
    )

    # Add a simple mirror
    jig_assy.add(
        outside_mirror(
            global_params["mirror_size"],
            global_params["mirror_thickness"],
            arrow_selector,
            annotate=annotate,
        ),
        name="mirror",
        color=cq.Color(0.75, 0.75, 0.75, 1.0),
        loc=cq.Location((0, 3.5, -0.2)),
        metadata={"explode_loc": cq.Location((0, 50, 0)), "export_format": None},
    )

    return jig_assy


# Show the object in the viewer if this script is being run by CQ-editor
if "show_object" in globals():
    # Create the jig assembly
    jig = jig_assembly(annotate=annotate)

    # Explode the assembly, if requested
    if explode:
        explode_assembly(jig)

    # Annotate the assembly with assembly arrows
    if annotate:
        jig = add_assembly_arrows(jig, arrow_scale_factor=1.25)

    show_object(jig)
