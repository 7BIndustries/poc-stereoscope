import cadquery as cq
import tools.helpers  # Add the parent directory to Python path regardless of execution environment

# import stereoscope
from cq_annotate import explode_assembly, add_assembly_arrows

# Display parameters
explode = True
annotate = True


def mirror_cutout(mirror_size, mirror_thickness):
    """
    The slots in the box that the mirrors fit into.
    """
    return cq.Workplane().box(
        mirror_size + 0.5, mirror_size * 2, mirror_thickness + 0.5
    )


def mirror_break(global_params, arrow_face_selector=None):
    """
    Generates the mirror break that applies constant pressure across the score
    line that is created using the mirror cut jig.
    """

    # Unpack the global parameters
    mirror_size = global_params["mirror_size"]
    inside_mirror_size = global_params["inside_mirror_size"]
    mirror_thickness = global_params["mirror_thickness"]

    # Overall shape
    mirror_break = cq.Workplane().box(
        mirror_size + 10, mirror_size - inside_mirror_size - 1.0, mirror_thickness + 2.5
    )

    # Mirror cutout
    mco = mirror_cutout(mirror_size, mirror_thickness)
    mco.val().locate(
        cq.Location(
            (0, mirror_size - (mirror_size - inside_mirror_size) / 2.0 + 0.75, 0)
        )
    )
    mirror_break = mirror_break.cut(mco)

    # Glass cutter guide channel
    # mirror_break = mirror_break.faces(">Z").workplane(invert=True).move(0, (-mirror_size + 3 + 4.65) / 2.0).rect(mirror_size + 10, 1.5).cutBlind(1.5)

    # Cutout to allow pushing the glass panel back out
    mirror_break = (
        mirror_break.faces("<Y")
        .workplane(centerOption="CenterOfBoundBox", invert=True)
        .rect(10, 10)
        .cutBlind(5.0)
    )

    # Tag the face to be annotated with an assembly arrow
    if arrow_face_selector != None:
        mirror_break.faces(arrow_face_selector).tag("arrow")

    return mirror_break


def break_assembly(
    global_params,
    inside_mirror,
    arrow_selector=">Y",
    arrow_part="mirror",
    annotate=False,
):
    """
    Generates the mirror break assembly with all the parts in the correct place.
    """

    break_assy = cq.Assembly()

    if arrow_part != "mirror":
        break_selector = arrow_selector
        mirror_selector = None
    else:
        break_selector = None
        mirror_selector = arrow_selector

    # Add the mirror and the break to the assembly
    break_assy.add(
        mirror_break(global_params, break_selector),
        name="break",
        color=cq.Color(1.0, 0.0, 0.0, 0.5),
        metadata={"export_format": "stl"},
    )
    break_assy.add(
        inside_mirror(
            global_params["mirror_size"],
            global_params["inside_mirror_size"],
            global_params["mirror_thickness"],
            mirror_selector,
            annotate=annotate,
        ),
        name="mirror",
        color=cq.Color(0.75, 0.75, 0.75, 1.0),
        loc=cq.Location((0, 13.75, -0.2)),
        metadata={"explode_loc": cq.Location((0, 15, 0)), "export_format": None},
    )

    return break_assy


# Show the object in the viewer if this script is being run by CQ-editor
if "show_object" in globals():
    # Create the jig assembly
    break_assy = break_assembly(annotate=annotate)

    # Explode the assembly, if requested
    if explode:
        explode_assembly(break_assy)

    # Annotate the assembly with assembly arrows
    if annotate:
        jig = add_assembly_arrows(break_assy, arrow_scale_factor=1.25)

    show_object(break_assy)
