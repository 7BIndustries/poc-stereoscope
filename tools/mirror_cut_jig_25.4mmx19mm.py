import os
import cadquery as cq
import stereoscope

# Parameters
export = True
export_base_path = "/tmp"

# Just to shorten the names a little bit
mirror_size = stereoscope.mirror_size
mirror_thickness = stereoscope.mirror_thickness


def mirror_cutout():
    """
    The slots in the box that the mirrors fit into.
    """
    return cq.Workplane().box(
        mirror_size + 0.2, mirror_size * 2, mirror_thickness + 0.5
    )


def jig():
    """
    Generates the jig that holds the mirror in position so that a straight
    and accurate cut can be performed.
    """

    # Overall shape
    jig = cq.Workplane().box(mirror_size + 10, mirror_size - 2, mirror_thickness + 2.5)

    # Mirror cutout
    mco = mirror_cutout()
    mco.val().locate(cq.Location((0, mirror_size / 2.0 + 2.0, 0)))
    jig = jig.cut(mco)

    # Glass cutter guide channel
    jig = (
        jig.faces(">Z")
        .workplane(invert=True)
        .move(0, (-mirror_size + 3 + 4.65) / 2.0)
        .rect(mirror_size + 10, 1.5)
        .cutBlind(1.5)
    )

    # Cutout to allow pushing the glass panel back out
    jig = (
        jig.faces("<Y")
        .workplane(centerOption="CenterOfBoundBox", invert=True)
        .rect(10, 10)
        .cutBlind(5.0)
    )
    return jig


jig_assy = cq.Assembly()
jig_assy.add(jig(), color=cq.Color(1.0, 0.0, 0.0, 0.5))
jig_assy.add(
    stereoscope.outside_mirror(),
    color=cq.Color(0.75, 0.75, 0.75, 1.0),
    loc=cq.Location((0, 2, -0.2)),
)

# If the user has requested an export, save all relevant files/formats
if export:
    cq.exporters.export(
        jig(), os.path.join(export_base_path, "mirror_cut_jig_19mm.stl")
    )

# Show the object in the viewer if this script is being run by CQ-editor
if "show_object" in globals():
    show_object(jig_assy)
