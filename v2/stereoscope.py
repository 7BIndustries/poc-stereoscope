import cadquery as cq
from math import sin, radians


def base(mirror_size=25):
    """
    The base that holds the mirrors in place.
    """

    # How thick the walls of the tube body are
    wall_thickness = 3.0

    # How much to add to slots to create a slip fit
    # This could vary per 3D printer and filament
    slot_tolerance = 0.75

    # Calculate the projected size of the mirror for things like cutouts and positioning
    projected_mirror_size = mirror_size * sin(radians(45))

    # The overall shape of the base
    base = (cq.Workplane()
            .rect(projected_mirror_size + wall_thickness, mirror_size + wall_thickness)
            .rect(projected_mirror_size, mirror_size)
            .extrude(mirror_size * 2.0)
    )

    # Image outlet cutout
    base = (base.faces(">X")
            .workplane(centerOption="CenterOfBoundBox")
            .rect(mirror_size - wall_thickness, mirror_size)
            .cutBlind(-wall_thickness)
    )

    return base


def stereoscope(mirror_size=25):
    """
    Generate the stereoscope assembly with all parts, including
    mirrors.
    """

    assy = cq.Assembly()
    assy.add(base(mirror_size=mirror_size), color=cq.Color(0.8, 0.0, 0.05, 1.0))

    return assy
