import cadquery as cq


def mirror(mirror_size=25, mirror_thickness=0.75):
    """
    An approximation of the mirror to be used in the stereoscope.
    :param mirror_size: The size of the mirror being used with the stereoscope.
    """

    mir = cq.Workplane().box(mirror_size, mirror_size, mirror_thickness)

    return mir
