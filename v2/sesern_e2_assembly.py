import cadquery as cq
from cameras.sesern_e2 import camera
from periscope import periscope
from stereoscope import stereoscope
from math import sin, radians


def sesern_e2(mirror_size=25):
    """
    Generates the Sesern E2 camera trap and stereoscope assembly.
    :param mirror_size: Size of the mirror being used, it is assumed
    that the mirror glass is square (same width and height).
    """

    # Calculate the projected size of the mirror for things like cutouts and positioning
    projected_mirror_size = mirror_size * sin(radians(45))

    assy = cq.Assembly()

    # Add the camera trap model
    cam = camera()
    assy.add(cam, name="camera", color=cq.Color(0.9, 0.5, 0.1, 1.0))

    # Get the dimensions of the camera trap so that it can be used to creaete calculated constraints
    cam_dims = cam.val().BoundingBox()

    # Add the periscope outlet mirror holder
    periscope_outlet = periscope(mirror_size=mirror_size)
    assy.add(
        periscope_outlet,
        name="periscope_outlet",
        loc=cq.Location(
            (0, mirror_size * 2.0 - projected_mirror_size - 5.0, cam_dims.zmax + 7.5),
            (1, 0, 0),
            -90,
        ),
    )

    # Add the periscope inlet mirror holder
    periscope_inlet = periscope(mirror_size=mirror_size)
    assy.add(
        periscope_inlet,
        name="periscope_inlet",
        loc=cq.Location(
            (0, mirror_size * 2.0 + projected_mirror_size + 15.0, cam_dims.zmax + 7.5),
            (1, 0, 0),
            90,
        ),
    )

    # Get the dimensions of the periscope sections for positioning
    peri_dims = periscope_inlet.toCompound().BoundingBox()

    # Add the stereoscope
    stereoscope_assy = stereoscope(mirror_size=mirror_size)
    stereo_dims = stereoscope_assy.toCompound().BoundingBox()
    assy.add(
        stereoscope_assy,
        name="stereoscope",
        loc=cq.Location(
            (-stereo_dims.zmax / 2.0, mirror_size * 2.0 + projected_mirror_size + 15.0 - mirror_size / 2.0 - 1.5, cam_dims.zmax + 7.5 + peri_dims.xmax - 1.0),
            (0, 1, 0),
            90
        )
    )

    return assy


e2 = sesern_e2()
