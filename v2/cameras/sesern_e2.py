import cadquery as cq


def camera():
    """
    Generates a model that is close enough to the real thing so that
    shims can be created for the camera trap.
    """

    # Main shape of the battery compartment
    cam = (
        cq.Workplane().rect(77.6, 132.2).workplane(offset=30.4).rect(82.0, 133.0).loft()
    )

    # Lip between the front and the back
    cam = (
        cam.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .rect(86.25, 137.25)
        .extrude(9.25)
    )

    # The base shape of the front camera face
    cam = (
        cam.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .rect(82.65, 133.0)
        .workplane(centerOption="CenterOfBoundBox", offset=7.5)
        .rect(78.75, 125.75)
        .loft()
    )

    # PIR sensor
    cam = (
        cam.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .moveTo(0.0, -27.0)
        .rect(50.0, 35.5)
        .extrude(13.5)
    )

    # IR flood emitter
    cam = (
        cam.faces(">Z[-2]")
        .workplane(centerOption="CenterOfBoundBox")
        .hLine(35 / 2.0)
        .polarLine(26.5, 45.0)
        .vLine(43.0)
        .hLine(-72.5)
        .vLine(-43.0)
        .polarLine(26.5, -45.0)
        .close()
        .workplane(offset=6.0)
        .move(0, 0.75)
        .hLine(35 / 2.0 - 0.75)
        .polarLine(26.5 - 0.75, 45.0)
        .vLine(43.0 - 1.5)
        .hLine(-72.5 + 2.5)
        .vLine(-43.0 + 1.5)
        .polarLine(26.5 - 0.75, -45.0)
        .close()
        .loft()
    )

    # Camera lens boss
    cam = (
        cam.faces(">Z[-2]")
        .workplane(centerOption="CenterOfBoundBox")
        .move(0.0, 37.375 - 26.6)
        .circle(26.6 / 2.0)
        .workplane(centerOption="CenterOfBoundBox", offset=4.25)
        .circle(25.0 / 2.0)
        .loft()
    )

    # Round the PIR sensor
    cam = cam.faces(">Z").edges("|Y").fillet(13.0)

    return cam
