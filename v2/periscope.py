import cadquery as cq
from mirror import mirror
from math import sin, radians

# All units are in mm unless otherwise specified


def periscope(direction="vertical", mirror_size=25, nut_width=5.5, nut_thickness=2.4):
    """
    Generates a periscope shim to help move the stereoscope to where it will not block
    PIR sensors, IR flood emitters, etc.
    :param direction: Direction that the periscope should be oriented in.
                      Options are "horizontal" and "vertical".
    :param mirror_size: The size of the mirror being used with the stereoscope.
    """

    # How thick the walls of the tube body are
    wall_thickness = 3.0

    # How much to add to slots to create a slip fit
    # This could vary per 3D printer and filament
    slot_tolerance = 0.75

    # Calculate the projected size of the mirror for things like cutouts and positioning
    projected_mirror_size = mirror_size * sin(radians(45))

    # The sizes for the mounting and adjustment ears
    ear_height = nut_width + 0.5
    ear_width = 10.0

    # Calculate the mirror center positions for hole alignment
    bottom_mirror_center = projected_mirror_size / 2.0 + 5.0

    assy = cq.Assembly()

    # Create the main tube body
    tube = (
        cq.Workplane()
        .rect(mirror_size + wall_thickness, projected_mirror_size + wall_thickness)
        .extrude(projected_mirror_size + 10.0)
    )

    # Mounting and adjustment ears
    ear_points = [
        (0.0, projected_mirror_size / 2.0),
        (0.0, -projected_mirror_size / 2.0),
    ]
    tube = (
        tube.faces(">X")
        .workplane(centerOption="CenterOfBoundBox")
        .center(
            (projected_mirror_size + wall_thickness) / 2.0 - (ear_height / 2.0), 0.0
        )
        .pushPoints(ear_points)
        .rect(ear_height, ear_width)
        .extrude(nut_width + 0.5)
    )
    tube = (
        tube.faces("<X")
        .workplane(centerOption="CenterOfBoundBox")
        .center(
            -(projected_mirror_size + wall_thickness) / 2.0 + (ear_height / 2.0), 0.0
        )
        .pushPoints(ear_points)
        .rect(ear_height, ear_width)
        .extrude(nut_width + 0.5)
    )

    # Hollow the tube
    tube = (
        tube.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .rect(mirror_size - 2.0, projected_mirror_size)
        .cutThruAll()
    )

    # Add the outlet hole for the bottom mirror projection
    tube = (
        tube.faces(">Y")
        .workplane(centerOption="CenterOfBoundBox")
        .rect(mirror_size - 2.0, projected_mirror_size)
        .cutBlind(-projected_mirror_size - wall_thickness / 2.0)
    )

    # Add the nut trap cutouts
    tube = (
        tube.faces(">X")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(ear_points)
        # .pushPoints([(0.0, projected_mirror_size / 2.0), (0.0, -projected_mirror_size / 2.0)])
        .rect(nut_thickness, nut_width)
        .cutBlind(-nut_width - 0.5)
    )
    tube = (
        tube.faces("<X")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(
            [(0.0, projected_mirror_size / 2.0), (0.0, -projected_mirror_size / 2.0),]
        )
        .rect(nut_thickness, nut_width)
        .cutBlind(-nut_width - 0.5)
    )

    # Add the adjustment screw holes
    outboard_pos = (mirror_size + wall_thickness + nut_width) / 2.0
    tube = (
        tube.faces("<Y[-3]")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(
            [
                (outboard_pos, -projected_mirror_size / 2.0),
                (outboard_pos, projected_mirror_size / 2.0),
                (-outboard_pos, projected_mirror_size / 2.0),
                (-outboard_pos, -projected_mirror_size / 2.0),
            ]
        )
        .hole(3.4)
    )

    # Add the mirror slot, which must break through to the outside
    mirror_cutout = mirror(
        mirror_size=mirror_size + slot_tolerance, mirror_thickness=0.75 + slot_tolerance
    )
    mirror_cutout = mirror_cutout.rotateAboutCenter((1, 0, 0), -45).translate(
        (0, 0, projected_mirror_size / 2.0 + 5.0)
    )
    tube = tube.cut(mirror_cutout)
    tube = tube.cut(mirror_cutout.translate((0, -wall_thickness, wall_thickness)))

    # Add the ears used to tie the sections together
    joiner_points = [
        (projected_mirror_size / 2.0 + wall_thickness + ear_width / 2.0, 0.0),
        (-projected_mirror_size / 2.0 - wall_thickness - ear_width / 2.0, 0.0),
    ]
    tube = (
        tube.faces(">Y")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(joiner_points)
        .rect(ear_width, ear_height)
        .extrude(-projected_mirror_size / 1.25)
    )

    # Add the nut pockets for the joiner ears
    joiner_nut_pocket_points = [
        (
            projected_mirror_size / 2.0 + wall_thickness + ear_width / 2.0 + 1.0,
            -ear_height / 4.0,
        ),
        (
            -projected_mirror_size / 2.0 - wall_thickness - ear_width / 2.0 - 1.0,
            -ear_height / 4.0,
        ),
    ]
    tube = (
        tube.faces("<Y[-3]")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(joiner_nut_pocket_points)
        .rect(nut_width, nut_thickness)
        .cutBlind(-projected_mirror_size / 2.0)
    )

    # Add the screw holes for the joiner ears
    joiner_screw_hole_points = [
        (
            projected_mirror_size / 2.0 + wall_thickness + ear_width / 2.0 + 1.0,
            -projected_mirror_size / 2.0 + wall_thickness / 2.0,
        ),
        (
            -projected_mirror_size / 2.0 - wall_thickness - ear_width / 2.0 - 1.0,
            -projected_mirror_size / 2.0 + wall_thickness / 2.0,
        ),
    ]
    tube = (
        tube.faces("<Z[-5]")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints(joiner_screw_hole_points)
        .circle(3.4 / 2.0)
        .cutThruAll()
    )

    # Add the main tube body to the assembly
    assy.add(tube, name="tube", color=cq.Color(0.04, 0.5, 0.67, 1.0))

    # Create the mirror and position it
    bottom_mirror = mirror(mirror_size)
    assy.add(
        bottom_mirror,
        name="mirror",
        color=cq.Color(0.75, 0.75, 0.75, 1.0),
        loc=cq.Location((0.0, 0.0, bottom_mirror_center), (1, 0, 0), -45),
    )

    return assy
