# poc-stereoscope

[![status-badge](https://ci.codeberg.org/api/badges/12262/status.svg)](https://ci.codeberg.org/repos/12262)

Manufacturing and assembly documentation is available [here](https://7bindustries.com/hardware/stereoscope/v1/index.html).

Open hardware project that is a proof-of-concept stereoscope attachment for phones, camera traps, webcams, etc to add depth sensing. The primary goal is to adapt camera traps to be able to better estimate wildlife populations and migration patterns, but other uses are also being explored.

![Built and assembled stereoscope image](docs/images/stereoscope_gallery.png)

## Status

In Development

## Research

* A research project to collect, process and analyze wildlife images from the stereoscope has been successfully funded on [Experiment.com](http://dx.doi.org/10.18258/51222%20).

## Licenses

* Software - [LGPL-3.0-or-later](LICENSES/LGPL-3.0-or-later.txt)
* Hardware - [CERN-OHL-P-2.0](LICENSES/CERN-OHL-P-2.0.txt)
* Documentation - [CC-BY-SA-4.0](LICENSES/CC-BY-SA-4.0.txt)
