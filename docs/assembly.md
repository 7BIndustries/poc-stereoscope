[50mmx50mm Mirror]:Parts.yaml#50mmx50mmMirror
[50mmx38mm Mirror]:Parts.yaml#50mmx38mmMirror
[M3 Hex Nut]:Parts.yaml#M3HexNut
[M3 Square Nut]:Parts.yaml#M3SquareNut
[M3x12mm Socket Head Screw]:Parts.yaml#M3x12SocketHeadScrew
[2.5mm Hex Wrench]:Tools.yaml#HexWrench2.5mm

# Assembly

## Introduction

This stereoscope is still in-progress, and so only a partial assembly is available at this time. Basic steps are outlined below to arrive at a stereoscope assembly that can be mated to a camera shim.

{{BOM}}

## Overall Assembly Diagram

This exploded view of the stereoscope assembly can be used as a shortcut, with detailed steps given below.

![Exploded Assembly View](./images/stereoscope_assembly_exploded_annotated.svg)

## Assemble the Center Piece {pagestep}

The center piece should have been printed in the [printing](printing.md) section. Nuts must be installed in the nut traps now so that this center piece can hold the base and front together.

An [M3 Square Nut]{Qty: 2, Cat: Part} is placed in each of the square pockets on the top of the center piece, and an [M3 Hex Nut]{Qty: 2, Cat: Part} is placed in each hex pocket on the bottom of the center piece. The following diagram shows the locations and direction of assembly.

![Stereoscope Center Assembly](./images/stereoscope_center_assembly_exploded_annotated.svg)
![Stereoscope Center Assembly with Hidden Lines](./images/stereoscope_center_assembly_exploded_annotated_hidden_view.svg)

The hex nut pockets are deep so that the same M3 bolt can be used in all places in this design. However, that means that a longer M3 bolt will be needed to pull and seat the hex nut fully into the pocket.

Make sure that the square nuts are pushed all the way down into their pockets as well. This can be done with a small screwdriver or hex wrench.

Once the nuts are seated in their pockets, set this part aside to use in a later assembly step.

## Insert the Inside Mirrors {pagestep}

The mirror cutting process should have been completed before attempting to assemble the stereoscope. If not, please return to [that process](./mirror_cutting.md) and complete it before coming back to this step.

Once the inside mirrors are cut, you should have two of the [50mmx38mm Mirror]{Qty: 2, Cat: Part}. Those should be inserted in the inner slots in the base, which should have been [3D printed](printing.md) in a previous section. The diagrams below show the correct orientation of the mirrors, and the correct slots to insert them into. The "M" character on the mirrors denotes the front of the mirror. It is important to get this orientation correct, or the stereoscope will not work. The reflecting side of the inside mirrors should face toward the bottom (mounting flange side) of the stereoscope base.

![Assembly Step Number 1](./images/stereoscope_assembly_step_1.svg)
![Assembly Step Number 1 Side View](./images/stereoscope_assembly_side_view_step_1.svg)

## Insert the Outside Mirrors {pagestep}

With the Inside Mirrors inserted, each [50mmx50mm Mirror]{Qty: 2, Cat: Part} can be inserted into the outside slots. Note that the mirrored side (denoted by the "M" character) is facing away from the mounting flange on the base in the following diagram.

![Assembly Step Number 2](./images/stereoscope_assembly_step_2.svg)
![Assembly Step Number 2 Side View](./images/stereoscope_assembly_side_view_step_2.svg)

## Insert the Assembled Center Piece {pagestep}

The center piece is inserted into the space between the inside mirrors.

![Center Piece Assembly](./images/stereoscope_center_fit_in_assembly_exploded_annotated.svg)
![Center Piece Assembly Side View](./images/stereoscope_center_fit_in_assembly_exploded_annotated_side_view.svg)

The assembly with the center piece should look like the following.

![Partial Assembly with Center Piece in Place](./images/stereoscope_center_piece_in_assembly_exploded_annotated.svg)

## Add the Center Piece Mounting Bolts {pagestep}

The bolts are now added through the side of the base to hold the center piece in place. Be careful not to over-tighten these screws as they will deform the plastic and put unnecessary stress on the mirrors inside the base. An [M3x12mm Socket Head Screw]{Qty: 2, Cat: Part} will be placed in each of the side holes. The following diagrams show the process.

![Screws Being Inserted Through Base to Hold Center Piece In Place](./images/stereoscope_center_piece_with_screws_in_assembly_exploded_annotated.svg)
![Screws Being Inserted Through Base to Hold Center Piece In Place Side View](./images/stereoscope_center_piece_with_screws_in_assembly_exploded_annotated_side_view.svg)

## Add the Front Piece {pagestep}

With the center piece secured in place, the front face can now be added along with the two screws that secure it in place. The following diagram shows how the front piece is oriented and secured.

![Front Piece Assembly with Screws](./images/stereoscope_front_piece_with_screws_in_assembly_exploded_annotated.svg)
![Front Piece Assembly with Screws Side View](./images/stereoscope_front_piece_with_screws_in_assembly_exploded_annotated_side_view.svg)

## Finished Stereoscope Assembly

The finished stereo assembly should now look like the following.

![Finished Stereoscope Assembly](./images/stereoscope_assembly.svg)

## Attach the Stereoscope to a Shim {pagestep}

This process will vary depending on the camera the stereoscope is being attached to, but a sample case using a shim for a Victure HC200 camera trap will be outlined here.

An [M3 Hex Nut]{Qty: 4, Cat: Part} needs to be placed in the hex nut pockets at the 4 corners of the stereoscope mounting flange. The center hole is not used for the HC200 shims. A mating [M3x12mm Socket Head Screw]{Qty: 4, Cat: Part} should be placed through the shim to engage with these nuts and hold the assembly together. Make sure that the counter-bore for the head of the cap screws is facing away from the stereoscope. The M3 cap screws can be tightened with a [2.5mm Hex Wrench]{Qty: 1, Cat: Tool}. A few views of this assembly step are shown below to give an idea of the correct orientation for all of the parts.

![Stereoscope and Shim Annotated and Exploded Assembly](./images/stereoscope_with_shim_assembly_exploded_annotated.svg)
![Stereoscope and Shim Annotated and Exploded Assembly Underneath](./images/stereoscope_with_shim_assembly_exploded_annotated_underneath.svg)

The shim is designed to be a press fit onto the front of the HC200 camera trap, but also has T-slots on each side so that rubber bands can be used to secure the stereoscope to the trap for longer-term testing.