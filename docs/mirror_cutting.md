[Mirror Cut Jig]:Tools.yaml#MirrorCutJig
[Mirror Break]:Tools.yaml#MirrorBreak
[50mmx50mm Mirror]:Parts.yaml#50mmx50mmMirror
[Glass Cutter]:Tools.yaml#GlassCutter
# Mirror Cutting

## Safety Note

Any time glass is being cut and broken there is a potential for injury. Be sure to wear safety equipment and follow safety procedures when performing these steps.

## Introduction

The inside mirrors of the stereoscope must be cut, unless a vendor can be found that will pre-cut mirrors to the correct size. 3D printed tools are included with this project to help make the process of scoring and cutting the mirrors easier. Be sure that the [tools have been printed](./printing.md#print-the-tools) before starting this section.

{{BOM}}

## Insert the Mirror into the Mirror Jig {pagestep}

Insert a [50mmx50mm Mirror]{Qty:1, Cat: Part} completely into the [Mirror Cut Jig]{Qty:1, Cat: Tool} as shown in the following illustration. The "M" character denotes the front side of the mirror glass, and this orientation is important when trying to achieve a clean score line. The arrow shows the direction of insertion.

![Mirror Cut Jig Assembly](./images/stereoscope_jig_assembly.svg)

When the mirror is inserted properly, it should look like the following, with the mirror showing in the notch at the bottom of the jig.

![Mirror Placed in Cut Jig](./images/mirror_placed_in_cut_jig.jpg)

## Score (Cut) the Mirror to Length {pagestep}

While applying moderate pressure downward with an index finger, draw the [Glass Cutter]{Qty: 1, Cat: Tool} through the channel to score the mirror. The idea is not to cut through the glass, but to score it so that it can be broken in the next step. The mirror should be cut all the way across, or the the glass may not break cleanly.

![Glass Being Cut in Jig](./images/mirror_being_cut_in_jig.jpg)

## Break the Glass {pagestep}

**Note:** Safety gear should be worn whenever working with glass, but especially during this step.

Insert the scored mirror glass into the [Mirror Break]{Qty:1, Cat: Tool} tool so that the score line is closest to the tool. The arrow again shows the direction of insertion.

![Mirror Break Assembly](./images/stereoscope_break_assembly.svg)

Once the mirror is fully inserted into the break, the assembly should look like this.

![Mirror Placed in Break Tool](./images/mirror_placed_in_break.jpg)

If the mirror has been scored properly all the way across, twisting the break tool downwards while holding the mirror should cause the mirror to break along the score line.

![Mirror Break Being Twisted](./images/stereoscope_break_operation.svg)

The shorter piece of mirror glass can be discarded, or better yet, set aside for a future project.

## Repeat for the Other Inside Mirror {pagestep}

Steps 1 through 3 should not be repeated to create a second [50mmx50mm Mirror]{Qty:1, Cat: Part}.

## Result

The result of this process should be two 50mmx38mm mirrors.
