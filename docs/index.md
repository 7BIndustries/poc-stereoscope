# Stereoscope

## Introduction

The Stereoscope is designed to add 3D depth perception to wildlife camera traps so that the distance from the camera to a subject being observed can be determined. Other metrics that may be possible to determine are the angle to the subject, and the number of subjects in a specific image.

## Contents

* [.](procurement.md){step}
* [.](printing.md){step}
* [.](mirror_cutting.md){step}
* [.](assembly.md){step}
* [.](acknowledgments.md){step}

A bill of materials page can be accessed from the sidebar, or these embedded links [ ]{BOM}.