# Printing

{{BOM}}

## Introduction

The Stereoscope has multiple 3D printed pieces which are then combined with off-the-shelf parts like mirrors and screws. These 3D printed parts must be created first before assembly can be attempted. Alternate formats are provided to manufacture the parts by other means (CNC machine, woodworking, etc).

## Obtain STL Files {pagestep}

* You can download the files directly from [the repository](https://codeberg.org/7BIndustries/poc-stereoscope/src/branch/main/docs/manufacturing_files). When the first release of the stereoscope occurs, a single zip file will be provided with all files.

## Print the Parts {pagestep}

The slicer software and settings used are left to the user, but some general settings and guidance will be outlined with each part below.

1. Base ([preview](manufacturing_files/base.stl){previewpage}, [download](manufacturing_files/base.stl), [step file](manufacturing_files/base.step)) - This is the body of the stereoscope that all other parts fit into or attach to. The base does have angled sides, but the angles are greater than 45 degrees and so no supports should be required. There is a mounting flange on the bottom face that should be positioned on the build plate, and which should help keep the base from lifting off the build plate during printing. Print settings used prototypes are outlined below.
    * Material: PETG (PLA should work fine as well, but will not be as durable)
    * Layer height: 0.15 mm
    * Infill: 20%
    * Brim: Yes
    * Perimeters: 2 (4 will add strength)
    * Supports: None
2. Center Piece ([preview](manufacturing_files/center_piece.stl){previewpage}, [download](manufacturing_files/center_piece.stl), [step file](manufacturing_files/center_piece.step)) - This part is used to hold the front of the stereoscope in place relative to the base, and can help hold the inner mirrors in place as well. Print settings used for prototypes are outlined below.
    * Material: PETG (PLA should work fine as well, but will not be as durable)
    * Layer height: 0.2 mm
    * Infill: 20%
    * Brim: Yes
    * Perimeters: 2
    * Supports: None
3. Front ([preview](manufacturing_files/front_cover.stl){previewpage}, [download](manufacturing_files/front_cover.stl), [step file](manufacturing_files/front_cover.step)) - Cover that blocks out stray light and can help hold mirrors in place. Print settings used for prototypes are outlined below.
    * Material: PETG (PLA should work fine as well, but will not be as durable)
    * Layer height: 0.2 mm
    * Infill: 20%
    * Brim: Yes
    * Perimeters: 2
    * Supports: Everywhere
4. Shim - This is the adapter that fits between the stereoscope and the camera the stereoscope is being adapted to. This will vary depending on the camera used and the use case, but a couple of examples are given below.
    * HC200 Centered Shim ([preview](manufacturing_files/hc200_centered_shim.stl){previewpage}, [download](manufacturing_files/hc200_centered_shim.stl), [step file](manufacturing_files/hc200_centered_shim.step)) - This shim is designed for the [Victure HC200](https://govicture.com/products/victure-hc200-trail-camera) camera trap, and assumes a centered camera sensor, which turned out not to be the case. It has tight tolerances and works as a press fit shim, but also includes T-shaped rubber band hooks to attach the stereoscope more securely.
    * Sample print settings for shims are given below. The main thing is to prevent the shim from lifting off the build surface during printing.
        * Material: PETG (PLA should work fine as well, but will not be as durable)
        * Layer height: 0.15mm (0.2 mm should work as well)
        * Infill: 20%
        * Brim: Yes
        * Perimeters: 2 (to 4)
        * Supports: On Build Plate

## Print the Tools {pagestep}

It is necessary to cut the inside mirrors to the correct size, and 3D printable tools have been provided to help the user do that more easily. Those tools are listed below. Safety precautions must be observed when working with glass, especially when cutting and breaking it.

1. Mirror Cut Jig ([preview](manufacturing_files/mirror_cut_jig_50mm_inside.stl){previewpage}, [download](manufacturing_files/mirror_cut_jig_50mm_inside.stl), [step file](manufacturing_files/mirror_cut_jig_50mm_inside.step)) - This jig has a channel to guide a glass cutter so that it is easier to get an accurate and straight cut. The inside mirrors needs to be very close to their specified length to avoid a hall-of-mirrors effect where the inside and outside mirrors overlap. Some post-processing is required with this print to remove supports, and the print settings for the prototype are given below.
    * Material: PETG (PLA should work as well, but may be too brittle at this wall thickness)
    * Layer height: 0.15mm
    * Infill: 20%
    * Brim: Yes
    * Perimeters: 4
    * Supports: Everywhere
2. Mirror Break ([preview](manufacturing_files/mirror_break_50mm_inside.stl){previewpage}, [download](manufacturing_files/mirror_break_50mm_inside.stl), [step file](manufacturing_files/mirror_break_50mm_inside.step)) - This mirror break is designed to be used after the mirror glass has been scored with the glass cutter and jig. The break allows even pressure along the score line. Print settings for the prototype are given below.
    * Material: PETG (PLA should work as well, but may be too brittle at this wall thickness)
    * Layer height: 0.15mm
    * Infill: 20%
    * Brim: Yes
    * Perimeters: 2 (or 4)
    * Supports: None
