# Acknowledgements

## People

There are multiple people who have contributed to this project.

* [Pen-Yuan Hsing](https://www.penonek.com/) - Pen's call to action after a presentation he gave on camera traps originally sparked the discussion that became this project. Pen has the field and data analysis experience to make sure that the stereoscope attachment is useful when put into service.
* [Joshua Pearce](https://www.eng.uwo.ca/electrical/faculty/pearce_j/index.html) - It was Joshua's idea to attempt optically splitting the camera sensor on camera traps into a stereo image. Joshua also has ideas on other applications for the stereoscope outside of wildlife monitoring.
* [Joshua Givans](https://www.appropedia.org/User:JGivans) - Joshua is independently recreating the stereoscope while providing feedback on the design, and has done the CAD work to create a custom shim to adapt the stereoscope for new uses.

## Funding

* Some funding has been provided for Pen's research through [Experiment.com](http://dx.doi.org/10.18258/51222%20), a scientific research crowdfunding platform.

## Projects

There are several open source projects that the design pipeline of this project relies on.

* [CadQuery](https://cadquery.readthedocs.io/en/latest/index.html)
* [GitBuilding](https://gitbuilding.io/)
* [CQ-editor](https://github.com/CadQuery/CQ-editor)
* [cq_annotate](https://github.com/jmwright/cq-annotate)
* [Python](https://www.python.org/)
