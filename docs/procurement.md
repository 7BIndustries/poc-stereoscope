# Procurement

Multiple parts must be purchased from vendors before the final assembly of the stereoscope is attempted. Those parts are outlined in the [bill of material]{BOM}, along with sample vendors they can be purchased from.